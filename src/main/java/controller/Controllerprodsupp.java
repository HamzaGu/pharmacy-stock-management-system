package Controllers;

import Entity.Categorie;
import Entity.Produit;
import Entity.ProduitSupp;
import Model.CategorieDAO;
import Model.ProduitDAO;
import Model.ProduitSuppDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

public class Controllerprodsupp {
	
	CategorieDAO ca=new CategorieDAO();
	ProduitDAO pr=new ProduitDAO();
	ProduitSuppDAO prsupp=new ProduitSuppDAO();
	
    @FXML
    private Button btnretoure;
    
    @FXML
    private Label lblidprod;
    @FXML
    private Label lbldesig;
    @FXML
    private Label lblprachar;
    @FXML
    private Label lblprvente;
    @FXML
    private Label lblqteprod;
    
    @FXML
    private Label lbllabo;
    @FXML
    private Label lblprincipe;
    @FXML
    private Label lblcat;
	 
	 @FXML
	    private void ReteuourMeth(ActionEvent event) {
		 if(event.getSource()==btnretoure) {
		 Categorie cat=  ca.chercherParId(Integer.parseInt((String) lblcat.getText()));
		
		 pr.insert(new Produit(lbldesig.getText(),Float.parseFloat(lblprachar.getText()),Float.parseFloat(lblprvente.getText()),Integer.parseInt(lblqteprod.getText()),lbllabo.getText(),lblprincipe.getText(),(Categorie) cat));
		 prsupp.delete(new ProduitSupp(lbldesig.getText(),Float.parseFloat(lblprachar.getText()),Float.parseFloat(lblprvente.getText()),Integer.parseInt(lblqteprod.getText()),lbllabo.getText(),lblprincipe.getText(),Integer.parseInt(lblcat.getText())));
		 }
		 
	 }
	 
	 
	 /*pour  supprimer button dans interface produit
	 
	 @FXML
	    private void Supprimerproduit(ActionEvent event) {
		 
		 
		 if(event.getSource()==btnsuprimer) {
			 
			 prsupp.insert(new ProduitSupp(tdesigprod.getText(),Float.parseFloat(tprachprod.getText()),Float.parseFloat(tventprod.getText()),Integer.parseInt(tqteprod.getText()),tlabprod.getText(),tprincprod.getText(),Integer.parseInt((String) combocatprod.getValue())); 
		 		
			 Produit i=tableprod.getSelectionModel().getSelectedItem();
			 
		     pr.delete(i);	
		 }
		 
	 }
	 */
}
